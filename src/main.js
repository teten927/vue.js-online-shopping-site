import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import firebase from 'firebase'
import slideUpDown from 'vue-slide-up-down'

Vue.config.productionTip = false
Vue.component('vue-slide-up-down', slideUpDown)

  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyAeoz5lr8zT7tvjf5BM1h344emhoNhQDv8",
    authDomain: "temmarushopping.firebaseapp.com",
    databaseURL: "https://temmarushopping.firebaseio.com",
    projectId: "temmarushopping",
    storageBucket: "temmarushopping.appspot.com",
    messagingSenderId: "266549696178",
    appId: "1:266549696178:web:30ca5465e06da7799c5bab",
    measurementId: "G-51VGPK235N"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
