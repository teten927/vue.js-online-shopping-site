let firebase;
let db;

// firestoreの読み込み
export function init(originFirebase) {
    firebase = originFirebase
    db = originFirebase.firestore()
}

// user取得関数
export async function getUser(id) {
    return ((await db.collection("users").where('id', '==', id).get()).docs)[0].data()
}
// user追加関数
export async function addUser(id, credit) {
    await db.collection('users').add({
        id: id,
        credit: credit,
        history: [],
        cart: []
    })
}
// user削除関数
export async function deleteUser(id) {
    let docId = (await db
        .collection("users")
        .where("id", "==", id)
        .get()).docs[0].id;
    db.collection("users")
        .doc(docId)
        .delete();
}
// user-id更新関数
export async function setUserId(id, newId) {
    let docId = (
        await db
            .collection("users")
            .where("id", "==", id)
            .get()
    ).docs[0].id;
    db.collection("users")
        .doc(docId)
        .update({ id: newId });
}
// user-cart更新関数
export async function setUserCart(id, cart) {
    let docId = (
        await db
            .collection("users")
            .where("id", "==", id)
            .get()
    ).docs[0].id;
    db.collection("users")
        .doc(docId)
        .update({ cart: cart });
}
// user-history追加関数
export async function addUserHistory(id, newHistory) {
    let docId = (
        await db
            .collection("users")
            .where("id", "==", id)
            .get()
    ).docs[0].id;
    let history = (await db.collection("users")
        .doc(docId)
        .get()).data().history;
    history = history.concat(newHistory)
    db.collection("users")
        .doc(docId)
        .update({ history: history });
}

// goods取得関数
export async function getGoods(category) {
    let goods = [];
    (await db.collection("goods").where('category', '==', category).get()).docs.forEach(good => {
        goods.push(good.data())
    });
    // imageを画像URL文字列から、ダウンロード用URLに変換
    for (let i = 0; i < goods.length; i++) {
        goods[i].image = await firebase
            .storage()
            .ref()
            .child(goods[i].image)
            .getDownloadURL()
    }
    return goods
}
// goods(その他カテゴリ)取得関数
export async function getOtherGoods() {
    let categories = await getCategories();
    let allGoods = [];
    let categoryGoods = [];
    (await db.collection("goods").get()).docs.forEach(good => {
        allGoods.push(good.data())
    });
    for (let category of categories) {
        (await db.collection("goods").where('category', '==', category).get()).docs.forEach(good => {
            categoryGoods.push(good.data())
        })
    }
    // categoriesに属していないgoodのみを取り出す
    let goods = allGoods.filter(good => {
        let check = true;
        for (let category of categoryGoods) {
            if (category.name === good.name) {
                check = false;
            }
        }
        return check;
    });
    // imageを画像URL文字列から、ダウンロード用URLに変換
    for (let i = 0; i < goods.length; i++) {
        goods[i].image = await firebase
            .storage()
            .ref()
            .child(goods[i].image)
            .getDownloadURL()
    }
    return goods;
}
// good取得関数
export async function getGood(name) {
    return ((await db.collection("goods").where('name', '==', name).get()).docs)[0].data()
}
// good在庫数更新関数
export async function setGoodStore(name, store) {
    let docId = (
        await db
            .collection("goods")
            .where("name", "==", name)
            .get()
    ).docs[0].id;
    db.collection("goods")
        .doc(docId)
        .update({ store: store });
}
// 購入goods在庫数チェック＆在庫数更新トランザクション処理
export async function checkStoreAndUpdateStore(goods) {
    let lackGoods = []; // 不足配列
    let docRefs = [];   // ドキュメント参照配列
    let afterStores = [];    // 在庫数配列
    for (let good of goods) {
        let docId = (((await db.collection("goods").where('name', '==', good.name).get()).docs)[0].id);
        docRefs.push(db.collection('goods').doc(docId))
    }
    await db.runTransaction(async transaction => {
        for (let i = 0; i < goods.length; i++) {
            let store = (await transaction.get(docRefs[i])).data().store;
            let afterStore = store - goods[i].count;
            afterStores.push(afterStore);
            // 在庫数 - 購入する数 < 0 のとき、不足配列に格納
            if (afterStore < 0) lackGoods.push("\n" + goods[i].name + "(在庫数:" + store + ")");
        }
        console.log(lackGoods)
        if (lackGoods.length > 0) return;
        for (let i = 0; i < docRefs.length; i++) {
            await transaction.update(docRefs[i], { store: afterStores[i] })
        }
    });
    return lackGoods;
}

// categories取得処理
export async function getCategories() {
    return (await db.collection("categories")
        .doc("categories")
        .get()).data().names;
}

// 認証user追加関数
export async function addAuthUser(id, password) {
    return await firebase
        .auth()
        .createUserWithEmailAndPassword(id, password)
}
// 認証userId更新関数
export async function updateAuthUserId(id) {
    await firebase.auth().currentUser.updateEmail(id);
}
// 認証userPassword更新関数
export async function updateAuthUserPassword(password) {
    await firebase.auth().currentUser.updatePassword(password);
}
// 認証user削除関数
export async function deleteAuthUser() {
    await firebase.auth().currentUser.delete();
}
// user認証関数
export async function authentificateUser(id, password) {
    await firebase
        .auth()
        .signInWithEmailAndPassword(id, password);
}